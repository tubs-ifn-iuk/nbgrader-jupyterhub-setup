# nbgrader with JupyterHub for teaching

This repository contains script to automatically install and configure nbgrader
and JupyterHub on a server.

# Installation and Setup

1. Make sure that you have at least Python 3.9 installed. (You might need to
   run `sudo apt remove python3-apt` after setting the alternatives)
```bash
# only if you do not already have Python 3.9 (or higher)
sudo apt install python3.9 python3.9-dev
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.9 1
sudo apt remove python3-apt
```
2. Copy all files to a remote server: (**Important: You need root access!**)
```
./deploy_files.sh root@demo-server
```
3. SSH to server and run the main script `run_installation.sh`
```bash
ssh root@demo-server
tmux  # to run it in a terminal multiplexer
./run_installation.sh
```
4. Adjust the paths to the SSL key/certificate in the
   `/srv/nbgrader/jupyterhub/jupyterhub_config.py` file
```python
c.JupyterHub.ssl_key = '/etc/ssl/private/<SSL_KEY.pem>'
c.JupyterHub.ssl_cert = '/etc/ssl/certs/<SSL_KEY.pem>'
```
(Don't forget to make sure that the permissions of the private key are set to
`600`.)
5. Export the LTI secret and key
```bash
export LTI_CLIENT_KEY=$(< /srv/nbgrader/jupyterhub/lti.key)
export LTI_CLIENT_SECRET=$(< /srv/nbgrader/jupyterhub/lti.secret)
```
If the key and secret are not available, you can create new ones by running
```bash
openssl rand -hex 32 > /srv/nbgrader/jupyterhub/lti.key
openssl rand -hex 32 > /srv/nbgrader/jupyterhub/lti.secret
```
Set the file permissions to `600` by running
```bash
chmod 600 /srv/nbgrader/jupyterhub/lti.key /srv/nbgrader/jupyterhub/lti.secret
```
6. Create and export `JUPYTERHUB_CRYPT_KEY`
```bash
export JUPYTERHUB_CRYPT_KEY=$(openssl rand -hex 32)
```
7. Add a new course. _The name of the course needs to match the name in
   StudIP!_ (You only need the name of the course, not _Vorlesung_ or any other
   prefix.)
```bash
./add_new_course.sh "Name of the Course as in StudIP"
```
8. Start the server
```bash
cd /srv/nbgrader/jupyterhub/
jupyterhub | tee "/var/log/jupyterhub.log"
```

# Adding SSL-certificates to the JupyterHub-Server

1. Add your domain's SSL privatekey and certificate to ```/etc/ssl/private/``` and ```/etc/ssl/certs/```

2. Change your JupyterHub configuration file located at ```/srv/nbgrader/jupyterhub/jupyterhub_config.py```

Modify the 2 lines according to your file names:
```Python
c.JupyterHub.ssl_key = '/etc/ssl/private/YOUR_PKEY_NAME.key'
c.JupyterHub.ssl_cert = '/etc/ssl/certs/YOUR_CERT_NAME.pem'
```



# Create a JupyterHub course and link it to StudIP

1. Log into JupyterHub server via SSH and change to `root` user in terminal.
2. Export `LTI_CLIENT_KEY`, `LTI_CLIENT_SECRET`, `JUPYTERHUB_CRYPT_KEY` by using the following commands:
```
export LTI_CLIENT_KEY=$(< /srv/nbgrader/jupyterhub/lti.key)
export LTI_CLIENT_SECRET=$(< /srv/nbgrader/jupyterhub/lti.secret)
export JUPYTERHUB_CRYPT_KEY=$(openssl rand -hex 32)
```
3. Go to the StudIP course site and copy the course name. (Note: Only copy the course name and omit `Vorlesung/Übung/Lecture/Tutorial/WS20xx/WS20xx` at the beginning and end.)

Example: 

❌ `Vorlesung: Optimierungs- und Spieltheorie in der Nachrichtentechnik [SoSe 2023]`

✅ `Optimierungs- und Spieltheorie in der Nachrichtentechnik`

4. Create the course on the JupyterHub server with the `add_new_course.sh` script. Example:
```
./add_new_course.sh "Optimierungs- und Spieltheorie in der Nachrichtentechnik"
```

5. Link the JupyterHub server with the StudIP course by:

- Enabling the LTI-Tool extension in the StudIP course.
- Open the LTI-Tool tab on the course site.
- Add a new section on the left menu bar. 

![](img/add-section.PNG "Title Text")

- Edit the section:

![](img/edit-section.PNG "Edit section")

   - Following properties need to be filled:
      - *Title*: `JupyterHub`
      - *Application URL*: `https://jupyter[1 or 2].ifn.ing.tu-bs.de:8000/hub/lti/launch`
      - *LTI tool consumer key*: copy content of file `/srv/nbgrader/jupyterhub/lti.key`
      - *LTI tool consumer secret*: copy content of file `/srv/nbgrader/jupyterhub/lti.secret`
      - *OAthOAuth signature method of the LTI tool*: HMAC-SHA1
      - ✅ *Send user data to LTI tool*
      - 🟩 *Display in an IFRAME on the page*
      - *Additional LTI parameters*: ` ` (empty)
   - Click on `Save`-Button

# Correction of assignments

1. Log into the instructor account of the course. Example: 
```
sudo su <instructor-signale-und-systeme>
```

2. Change into the course directory. Example:
```
cd signale-und-systeme
```

3. Autograde the submitted assignment. The submitted assignments need to be downloaded/collected before (which can be done via the Web interface). Example:
```
nbgrader autograde Programmieruebung3-2023
```

*Note: In case, you do not know the name of the assignment: you can list the available assignments with the following command: `nbgrader list`. It is also important that your assigment name does not contain special characters like whitespace and brackets.*

4. After autograding, the correction/feedback can be distributed via the Web interface of JupyterHub.
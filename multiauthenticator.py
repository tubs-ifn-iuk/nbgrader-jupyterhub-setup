from urllib.parse import urlsplit

from tornado import gen
from tornado.escape import url_escape
from tornado.httputil import url_concat
from asyncio import Future

from traitlets import (
    Unicode, Integer, Dict, TraitError, List, Bool, Any,
    Type, Set, Instance, Bytes, Float,
    observe, default,
)

from jupyterhub.auth import Authenticator
from jupyterhub.handlers.login import LoginHandler, LogoutHandler

from firstuseauthenticator import FirstUseAuthenticator
from firstuseauthenticator.firstuseauthenticator import CustomLoginHandler, ResetPasswordHandler
from ltiauthenticator import LTIAuthenticator, LTIAuthenticateHandler
import pdb


class MultiAuthenticator(Authenticator):
    enable_lti = Bool(True, help='Enable LTI authorization to Stud.IP').tag(config=True)
    enable_firstuse = Bool(True, help='Enable FirstUse authorization').tag(config=True)

    lti_class = Type(LTIAuthenticator, Authenticator, help='Must be an authenticator').tag(config=True)
    firstuse_class = Type(FirstUseAuthenticator, Authenticator, help='Must be an authenticator').tag(config=True)

    lti_authenticator = Instance(LTIAuthenticator)
    firstuse_authenticator = Instance(FirstUseAuthenticator)

    @default('lti_authenticator')
    def _default_lti_authenticator(self):
        return self.lti_class()

    @default('firstuse_authenticator')
    def _default_firstuse_authenticator(self):
        return self.firstuse_class()

    def get_handlers(self, app):
        h = []
        if self.enable_lti:
            handlers = dict(self.lti_authenticator.get_handlers(app))
            h += [
                (r'/lti/launch', LTIAuthenticateHandler)
            ]
        if self.enable_firstuse:
            handlers = dict(self.firstuse_authenticator.get_handlers(app))
            h += [
                (r'/login', CustomLoginHandler),
                (r'/auth/change-password', ResetPasswordHandler)
            ]
        return h

    #@gen.coroutine
    def authenticate(self, handler, data):
        """
        Delegate authentication to the appropriate authenticator
        """
        if isinstance(handler, LTIAuthenticateHandler):
            return self.lti_authenticator.authenticate(handler, data)
        else:
            return self.firstuse_authenticator.authenticate(handler, data)

#!/usr/bin/env bash

set -e
source utils.sh
source config_path.sh

add_new_course () {
	local course=$(python3 sanitize_course_name.py "${1}")

    echo "Setting up course '${course}'..."
    source setup_course.sh
    setup_course "${jupyterhub_root}" "$course"
}

add_new_course "${@}"

#!/usr/bin/env bash

set -e

source config_path.sh

# Import helper functions.
source utils.sh

install_dependencies () {
    echo "Installing dependencies..."
    apt update
    apt install -y git npm tmux
    npm install -g configurable-http-proxy
    apt install -y python3-pip
    pip3 install -U jupyter
    pip3 install -U jupyterhub
    pip3 install -U jupyterhub-firstuseauthenticator
	#pip3 install -U jupyterhub-idle-culler
	pip3 install -U jupyterhub-systemdspawner
	pip3 install -U jupyterhub-ltiauthenticator
	pip3 install -U jupyter-resource-usage
    pip3 install -U -r requirements.txt
}

install_nbgrader () {
    local nbgrader_root="${1}"
    local exchange_root="${2}"

    echo "Installing nbgrader in '${nbgrader_root}'..."

    # Clone nbgrader.
    if [ ! -d "${nbgrader_root}" ]; then
        mkdir "${nbgrader_root}"
        cd "${nbgrader_root}"
        git clone https://github.com/jupyter/nbgrader .
    fi

    # Update git repository.
    cd "${nbgrader_root}"
    git checkout master
    git pull

    # Install requirements and nbgrader.
    pip3 install -U -r requirements.txt -e .

    # Install global extensions, and disable them globally. We will re-enable
    # specific ones for different user accounts in each demo.
    jupyter nbextension install --symlink --sys-prefix --py nbgrader --overwrite
    jupyter nbextension disable --sys-prefix --py nbgrader
    jupyter serverextension disable --sys-prefix --py nbgrader

    # Everybody gets the validate extension, however.
    jupyter nbextension enable --sys-prefix validate_assignment/main --section=notebook
    jupyter serverextension enable --sys-prefix nbgrader.server_extensions.validate_assignment

	# Enable assignment list for all users
	jupyter nbextension enable --sys-prefix assignment_list/main --section=tree
    jupyter serverextension enable --sys-prefix nbgrader.server_extensions.assignment_list

    # Reset exchange.
    rm -rf "${exchange_root}"
    setup_directory "${exchange_root}" ugo+rwx

    # Remove global nbgrader configuration, if it exists.
    rm -f /etc/jupyter/nbgrader_config.py

	# Create custom global nbgrader configuration
	echo "c = get_config()" > "${jupyter_env_wide}/nbgrader_config.py"
}

restart_demo () {
    #local course="${1}"
	# ${2}: Studentenliste.csv

    install_dependencies
    setup_directory "${srv_root}" ugo+r
    install_nbgrader "${nbgrader_root}" "${exchange_root}"
    cd "${root}"

    # Setup JupyterHub
	echo "Setting up JupyterHub"
    setup_jupyterhub "${jupyterhub_root}"
	cp "global_nbgrader_config.py" "${jupyter_env_wide}/nbgrader_config.py"

    #echo "Setting up course '${course}'..."
    #source setup_course.sh
    #setup_course "${jupyterhub_root}" "$course"

    # Run JupyterHub.
    #cd "${jupyterhub_root}"
    #jupyterhub
}

restart_demo "${@}"

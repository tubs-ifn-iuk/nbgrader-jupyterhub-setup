#!/usr/bin/env bash

# $1: CSV-File

set -e
source utils.sh

for i in $(awk -F "\"*;\"*" '{print $6}' "${1}"); do
	if [[ "$i" != "Nutzernamen" ]]; then
		echo "Removing user: $i"
		remove_user "$i"
	fi
done
remove_user "instructor"

#!/usr/bin/env bash

set -e

#INSTRUCTOR="instructor"

get_token () {
    local jupyterhub_root="${1}"
    local user="${2}"
    local currdir="$(pwd)"
    cd "${jupyterhub_root}"
    local token=$(jupyterhub token "${2}")
    cd "${currdir}"
    echo "$token"
}

setup_course () {
	# ${1}: JupyterHubRoot
	# ${2}: course name
    local jupyterhub_root="${1}"
	export course_name="${2}"

	export INSTRUCTOR="instructor-${course_name}"
    local runas="sudo -u $INSTRUCTOR"


	#echo "course_id = '${2}'" >> "${jupyterhub_root}/jupyterhub_config.py" #TODO

    make_user "$INSTRUCTOR"  # additional instructor account

	local config="${jupyterhub_root}/jupyterhub_config.py"
	python3 modify_config.py -i "${INSTRUCTOR}" --add_admin --config="${config}"
	local token=$(get_token "${jupyterhub_root}" "${INSTRUCTOR}")
	python3 modify_config.py -i "${INSTRUCTOR}" -c "${course_name}" -a "${token}" --config="${config}"
	#export JUPYTERHUB_USER="${INSTRUCTOR}"
	#export JUPYTERHUB_API_TOKEN="${token}"

    # Setup instructor nbgrader configuration.
	echo "c = get_config()" > "${INSTRUCTOR}_nbgrader_config.py"
	echo "c.CourseDirectory.root = '/home/${INSTRUCTOR}/${course_name}'" >> "${INSTRUCTOR}_nbgrader_config.py"
	echo "c.CourseDirectory.course_id = '${course_name}'" >> "${INSTRUCTOR}_nbgrader_config.py"
	echo "c.LateSubmissionPlugin.penalty_method = 'zero'" >> "${INSTRUCTOR}_nbgrader_config.py"
    setup_nbgrader "$INSTRUCTOR" "${INSTRUCTOR}_nbgrader_config.py"
    create_course "$INSTRUCTOR" "${course_name}"

    # Enable extensions for instructor.
    enable_create_assignment "$INSTRUCTOR"
    enable_formgrader "$INSTRUCTOR"
    enable_assignment_list "$INSTRUCTOR"
	enable_course_list "${INSTRUCTOR}"
    chmod 770 "/home/${INSTRUCTOR}"
}

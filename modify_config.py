import ast
import argparse
#import astunparse

#CONF = "jupyterhub_config.py"

def main(**kwargs):
    config_file = kwargs['config']
    with open(config_file) as conf_file:
        _conf_content = conf_file.read()
    _conf = ast.parse(_conf_content, config_file)

    only_add_admin = kwargs['add_admin']
    if not only_add_admin:
        grader_group = "formgrade-{}".format(kwargs['course_name'])
        student_group = "nbgrader-{}".format(kwargs['course_name'])

    class Transformer(ast.NodeTransformer):
        def generic_visit(self, node):
            super().generic_visit(node)
            if isinstance(node, ast.Assign):
                _targets = node.targets
                if not _targets:
                    return node
                _target = _targets[0]
                if isinstance(_target, ast.Name):
                    return node
                elif isinstance(_target, ast.Attribute):
                    _name = _targets[0].attr
                    if only_add_admin:
                        if _name == "admin_users":
                            new_admin = ast.Constant(kwargs['instructor'])
                            node.value.elts.append(new_admin)
                    else:
                        if _name == "services":
                            port = 10000 - len(node.value.elts)
                            new_service = """{{'name': '{course_name}',
                                            'url': 'http://127.0.0.1:{port}',
                                            'command': ['jupyterhub-singleuser', '--group={grader_group}'],
                                            'user': '{instructor}',
                                            'cwd': '/home/{instructor}',
                                            'api_token': '{api_token}'
                                            }}""".format(**kwargs, grader_group=grader_group, port=port)
                            new_service = ast.parse(new_service)
                            node.value.elts.append(new_service)
                        elif _name == "load_groups":
                            new_groupname = ast.Constant(grader_group)
                            new_groupmembers = ast.parse("['{}']".format(kwargs['instructor']))
                            node.value.keys.append(new_groupname)
                            node.value.values.append(new_groupmembers)
                            new_stud_groupname = ast.Constant(student_group)
                            new_stud_groupmembers = ast.parse("[]")
                            node.value.keys.append(new_stud_groupname)
                            node.value.values.append(new_stud_groupmembers)
                else:
                    return node
            return node

    transformer = Transformer()
    new_conf = transformer.visit(_conf)
    #OUT_FILE = "new_jupyterhub_config.py"
    with open(config_file, 'w') as out_file:
        #out_file.write(astunparse.unparse(new_conf))
        out_file.write(ast.unparse(new_conf))
    return new_conf

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--course_name")
    parser.add_argument("-i", "--instructor", required=True)
    parser.add_argument("-a", "--api_token")
    parser.add_argument("--config", required=True)
    parser.add_argument("--add_admin", action="store_true")
    args = vars(parser.parse_args())
    new_conf = main(**args)

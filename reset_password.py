import dbm

def list_users(database, username):
    with dbm.open(database, 'r') as db:
        k = db.firstkey()
        while k != None:
            print(k)
            k = db.nextkey(k)
        if username in db:
            print("\nUser '{}' is in the database".format(username))
        else:
            print("\nUser '{}' not found".format(username))

def reset_user_password(database, username):
    with dbm.open(database, 'w') as db:
        k = db.firstkey()
        while k != None:
            print(k)
            k = db.nextkey(k)
        if username in db:
            del db[username]
            print("Successfully reset password for user: {}".format(username))
        else:
            raise KeyError("The user is not in the database")

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--database", default="passwords.dbm")
    parser.add_argument("-l", dest="only_list", action="store_true")
    parser.add_argument("username")
    args = vars(parser.parse_args())
    only_list = args.pop("only_list")
    if only_list:
        list_users(**args)
    else:
        reset_user_password(**args)

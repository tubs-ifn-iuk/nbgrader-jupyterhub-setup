import sys
import os
from subprocess import check_call

from jupyterhub.auth import PAMAuthenticator
from firstuseauthenticator import FirstUseAuthenticator
from ltiauthenticator import LTIAuthenticator

sys.path.append('/srv/nbgrader/jupyterhub')
from multiauthenticator import MultiAuthenticator
import create_user
from sanitize_course_name import sanitize_course_id

c = get_config()

c.JupyterHub.authenticator_class = MultiAuthenticator
c.MultiAuthenticator.lti_authenticator = LTIAuthenticator
c.MultiAuthenticator.lti_authenticator.consumers = {
    os.environ['LTI_CLIENT_KEY']: os.environ['LTI_CLIENT_SECRET']
}
c.MultiAuthenticator.firstuse_authenticator = FirstUseAuthenticator

c.Authenticator.admin_users = []

c.Authenticator.blocked_users = ['root']
c.Authenticator.enable_auth_state = True

c.JupyterHub.spawner_class = 'systemdspawner.SystemdSpawner'
c.SystemdSpawner.username_template = '{USERNAME}'
c.SystemdSpawner.mem_limit = '400M'
c.SystemdSpawner.dynamic_users = False
c.SystemdSpawner.disable_user_sudo = True

c.JupyterHub.services = []
c.JupyterHub.load_groups = {}
c.JupyterHub.template_paths = ['/srv/nbgrader/jupyterhub/templates/']

c.JupyterHub.ssl_key = '/etc/ssl/private/ifn_key.pem'
c.JupyterHub.ssl_cert = '/etc/ssl/certs/ifn_cert.pem'

def userdata_hook(spawner, auth_state):
    spawner.userdata = auth_state

def pre_spawn_hook(spawner):
    username = spawner.user.name
    userdata = spawner.userdata
    if userdata is None:
        return
    course_id = sanitize_course_id(userdata['context_title'])
    instructor_name = 'instructor-{}'.format(course_id)
    print('User: {}\nData: {}\nCourse: {}'.format(username, userdata, course_id, instructor=instructor_name))
    try:
        create_user.create_unix_user(username)
        create_user.add_student_to_course(username, userdata, course_id, instructor=instructor_name)
    except Exception as e:
        print(f'{e}')

c.Spawner.auth_state_hook = userdata_hook
c.Spawner.pre_spawn_hook = pre_spawn_hook

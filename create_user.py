import shutil
import os
from subprocess import check_call

from nbgrader.apps import NbGraderAPI
from traitlets.config import Config


def recursive_chown(path, owner):
    for dirpath, dirnames, filenames in os.walk(path):
        shutil.chown(dirpath, owner, owner)
        for filename in filenames:
            shutil.chown(os.path.join(dirpath, filename), owner, owner)


CUSTOM_TEMPLATE_DIR = "/srv/nbgrader/jupyterhub/"
def create_unix_user(username):
    try:
        check_call(['useradd', '-m', username])
    except Exception as e:
        print(f'Could not add user: {e}')
    _home_path = os.path.join("/home", username)
    _jupyter_config_path = os.path.join(_home_path, ".jupyter", "custom")
    #os.makedirs(_home_path, exist_ok=True)
    try:
        shutil.copytree(os.path.join(CUSTOM_TEMPLATE_DIR, "custom"), _jupyter_config_path)
    except FileExistsError:
        print("Custom CSS dir already exists")
    recursive_chown(_home_path, username)
    #os.chmod(_home_path, 770)


def add_student_to_course(username, userdata, course_id, instructor="instructor"):
    student_data = {}
    student_data['first_name'] = userdata['lis_person_name_given']
    student_data['last_name'] = userdata['lis_person_name_family']
    student_data['email'] = userdata['lis_person_contact_email_primary']
    _studip_name = userdata['lis_person_sourcedid']

    config = Config()
    config.CourseDirectory.course_id = course_id
    config.CourseDirectory.root = os.path.join("/home", instructor, course_id)
    api = NbGraderAPI(config=config)
    with api.gradebook as gb:
        gb.update_or_create_student(username, **student_data)

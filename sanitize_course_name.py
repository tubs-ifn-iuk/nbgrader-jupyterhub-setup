import re

def sanitize_course_id(name):
    name = name.replace(" ", "-")
    name = name.lower()
    name = re.sub(r'[^-a-z0-9]+', "", name)
    name = name.encode("ascii", "ignore")
    name = name.decode("ascii")
    return name


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("course_id")
    args = vars(parser.parse_args())
    new_name = sanitize_course_id(args['course_id'])
    print(new_name)

#!/usr/bin/env bash

# $1: courseName
# $2: CSV File

set -e
source utils.sh
source setup_course.sh

# Read updated CSV list and only add new users
while read line; do
	studData=($(echo $line | awk -F "\"*;\"*" '{print $6,$3,$4}'))
	if [[ "${studData[0]}" != "Nutzernamen" ]]; then
		userName="${studData[0]}"
		firstName="${studData[1]}"
		lastName="${studData[2]}"
		if id "${userName}" &> /dev/null; then
			echo "User ${userName} already exists."
		else
			echo "Creating user: ${userName}"
			make_user "${userName}"
			enable_assignment_list "${userName}"  # Enable extensions for student.
			chmod 770 "/home/${userName}"
			${runas} nbgrader db student add "${userName}" --first-name "${firstName}" --last-name "${lastName}" --course-dir="/home/$INSTRUCTOR/${1}"
		fi
	fi
done < "${2}"

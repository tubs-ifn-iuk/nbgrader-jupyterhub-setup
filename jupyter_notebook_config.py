c.NotebookApp.terminals_enabled = False
# shutdown the server after no activity for half an hour
c.NotebookApp.shutdown_no_activity_timeout = 30 * 60
# shutdown kernels after no activity for 10 minutes
c.MappingKernelManager.cull_idle_timeout = 10 * 60
# check for idle kernels every two minutes
c.MappingKernelManager.cull_interval = 2 * 60
